package com.example.tonytuan.myapplication;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rv;
    private PromotionAdapter promotionAdapter;
    private List<Deal> lstDeals = new ArrayList<>();
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv = findViewById(R.id.rv);

        activity = this;

        (new MyAsynTask()).execute();

    }

    class MyAsynTask extends AsyncTask<Void, Void, String> {
        private int INCREASING_SECONDS = 15;
        private int DURATION = 60; // 1 minute
        @Override
        protected String doInBackground(Void... voids) {
            String json = loadJSONFromAsset();
            return json;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray m_jArry = new JSONArray(s);
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    String name = jo_inside.getString("name");
                    Double price = jo_inside.getDouble("price");
                    String thumbnail = "file:///android_asset/data/images/" + jo_inside.getString("thumbnail");

                    Calendar now = Calendar.getInstance();
                    now.add(Calendar.SECOND, (i+1) * INCREASING_SECONDS);
                    long start = now.getTimeInMillis();

                    now.add(Calendar.SECOND, DURATION);
                    long end = now.getTimeInMillis();

                    Deal deal = new Deal(name, price, thumbnail, start, end);
                    lstDeals.add(deal);

                }

                promotionAdapter = new PromotionAdapter(lstDeals, activity);
                rv.setAdapter(promotionAdapter);
                rv.setLayoutManager(new GridLayoutManager(activity, 2));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private String loadJSONFromAsset(){
            String json = null;
            try{
                InputStream is = getAssets().open("data/deals.json");
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                json = new String(buffer, "UTF-8");
            } catch (IOException ex){
                ex.printStackTrace();
            }

            return json;
        }
    }

}
