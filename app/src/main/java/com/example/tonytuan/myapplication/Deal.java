package com.example.tonytuan.myapplication;

public class Deal {
    private String name;
    private Double price;
    private String thumbnail;
    private long start;
    private long end;
    private long diff;

    public Deal(String name, Double price, String thumbnail, long start, long end) {
        this.name = name;
        this.price = price;
        this.thumbnail = thumbnail;
        this.start = start;
        this.end = end;
        this.diff = end - start;
    }

    public long getDiff() {
        return diff;
    }

    public void setDiff(long diff) {
        this.diff = diff;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
